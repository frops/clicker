
/*
 * GET home page.
 */
var model = require("../lib/model.js");

exports.index = function(req, res){
  res.render('index', { title: 'Clicker by Sopost', point: 14, username: 'frops'});
};

exports.register = function(req, res) {
    console.log(req.body);
    if (req.body.username) {
        model.register({username: req.body.username}, req, res, function(data) {
            writeJSON(res, data);
        });
    } else {
        writeErrorJSON(res, 'Не указано имя');
    }
};

exports.checkAuth = function(req, res) {
    model.checkAuth(req, res, function(data) {
        writeJSON(res, data);
    });
};

exports.click = function(req, res) {
    model.click(req, res, function(data) {
        writeJSON(res, data);
    });
}

exports.rating = function(req, res) {
    model.rating(req, res, function(data) {
        writeJSON(res, data);
    });
}

function writeErrorJSON(res, message) {
    writeJSON(res, {error: true, message: message});
}

function writeJSON(res, data)
{
    res.writeHead(200, {"Content-Type": "application/json"});
    res.end(JSON.stringify(data, null, 2))
}
