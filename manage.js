var mongoose = require('./node_modules/mongoose'),
	async = require('async'),
	User = require('./models/user'),
	log = require('./utils/log')(null, module),
	config = require('./config');


function openConnection(cb) {
	mongoose.connection.on('open', function () {
		log.info('connected to database ' + config.get('db:name'));
		cb();
	});
}