var MongoClient = require('mongodb').MongoClient
, assert = require('assert')
, ObjectId = require('mongodb').ObjectID
, url = 'mongodb://localhost:/test';


exports.register = function(data, req, res, callback, next) {
    MongoClient.connect(url, function(err, db) {
      assert.equal(null, err);

      findUser(data.username, db, function(user) {
          if (user == null) {
              db.collection('users').insertOne(
                  {
                      "username": data.username,
                      "count": 0,
                      "items": {
                          "multiplier": 1.00
                      }
                  }, function(err, result) {
                   assert.equal(err, null)
                   console.log("Inserted a document into the user collection.")
                   console.log(result.ops[0], 'register')

                   var d = new Date()
                   d.setDate(d.getDate() + (31 * 12))

                   res.cookie('userId', result.ops[0]._id, {expires: d, domain: "clicker.sopost.ru"})
                   db.close()

                   callback({status: 'success', user: result.ops[0]})
                 }
             );
         } else {
             console.log(user.user, 'user exists 2');
             db.close()

             var d = new Date()
             d.setDate(d.getDate() + (31 * 12))

             res.cookie('userId', user.user._id, {expires: d, domain: "clicker.sopost.ru"})
             callback({status: 'success', user: user.user})
         }
      });
    });
}

exports.checkAuth = function(req, res, callback) {
    checkAuth(req, res, callback)
}

exports.click = function(req, res, callback) {
    checkAuth(req, res, function(data) {
        if (data.user) {
            MongoClient.connect(url, function(err, db) {
              assert.equal(null, err);

              var newCount = parseInt(getRandomCount());//Math.pow(10, Math.floor(log10(data.user.count))) / 10;
              console.log(newCount, 'nccc');

              var cursor = db.collection('users').updateOne({"_id": ObjectId(req.cookies.userid)}, {$inc: {"count": newCount}}, function(err, result) {
                  db.close();
                  if (err) {
                      callback({status: 'fail'});
                  } else {
                      callback({status: 'success', count: newCount});
                  }
                });
            });

        }
    });
}

exports.rating = function(req, res, callback) {
    if (req.cookies.userid) {
        MongoClient.connect(url, function(err, db) {
          assert.equal(null, err);

          var cursor = db.collection('users').find().sort({"count": -1}).limit(10);
          var users = [];

          cursor.each(function(err, doc) {


              if (doc != null) {
                 users.push({username: doc.username, count: doc.count});
              } else {
                 db.close();
                 callback({status: 'success', users: users});
              }
          });
        });
    } else {
        callback({user: null});
    }
}

var findUser = function(username, db, callback) {
    var cursor = db.collection('users').find({"username": username});
    var user = null;

    cursor.each(function(err, doc) {
        if (doc != null) {
            user = {status: 'success', user: doc};
        } else {
            callback(user);
        }
    });
}

var checkAuth = function(req, res, callback) {
    if (req.cookies.userid && req.cookies.userid != 0) {
        MongoClient.connect(url, function(err, db) {
          assert.equal(null, err);

          var cursor = db.collection('users').find({"_id": ObjectId(req.cookies.userid)});
          var user = null;

          cursor.each(function(err, doc) {
              if (doc != null) {
                 db.close();
                 callback({status: 'success', user: doc});
              }
          });
      })
    } else {
        callback({user: null});
    }
}

var log10 = function(val) {
  return Math.log(val) / Math.LN10;
}

var chances = [
    {value: "1", chance: 40.00},
    {value: "2", chance: 24.00},
    {value: "5", chance: 18.00},
    {value: "10", chance: 9.00},
    {value: "100", chance: 8.40},
    {value: "1000", chance: 0.6},
];

var getRandomCount = function() {
    var rand = Number((Math.random() * (0.0001 - 99.9999) + 99.9999).toFixed(4));
    console.log(rand, 'rand');

    var chance = 0.0000;
    var result = null;

    chances.forEach(function(v) {
        chance += v.chance;
        if (rand < chance && result === null) {
            console.log(rand + ', ' + chance, 'rr')
            result = v.value;
        }
    });

    if (!result) {
        return chances[0].value;
    }

    return result;
}
