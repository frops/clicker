#!/usr/bin/env bash

# Update remote package metadata
apt-get update -q
#apt-get upgrade -y

# Install deb dependencies
apt-get install \
	nodejs \

echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
apt-get -y install mysql-server

sed -i.bak -e "s/bind-address/#bind-address/g" /etc/mysql/my.cnf
service mysql restart
mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY ''; SET PASSWORD FOR 'root'@'localhost' = PASSWORD(''); FLUSH PRIVILEGES;"

mysql -uroot < /vagrant/data/dump.sql

sudo rm -r /etc/nginx/sites-available/default
cat /vagrant/vagrant/www.conf >> /etc/nginx/sites-enabled/www.conf
service nginx restart

sed -i.bak -e "s/short_open_tag = Off/short_open_tag = On/g" /etc/php5/fpm/php.ini
sed -i.bak -e "s/;date.timezone =/date.timezone = America\/New_York/g" /etc/php5/fpm/php.ini
service php5-fpm restart

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

composer global require "fxp/composer-asset-plugin:~1.1.1"