$(function() {
    var user = {
        userId: getCookie('userId'),
        userName: '',
        count: 0,
        block: false,
        lastTime: 0,
        timer: null
    };

    if (!user.userId) {
        //username = makeid();
        $('#regModal').modal();
    } else {
        checkAuth();
    }

    // Events

    $("#signin").on('click', function() {
        register($("#formUsername").val());
    });

    $("#click").on('click', function() {
        click();
    });

    // Functions

    function startRating() {
        getRating();
        setInterval(function() {
            getRating();
        }, 1000);
    }

    function fillUser() {
        $("#username").html(user.userName);
        $("#count").html(numberWithCommas(user.count));
        startRating();
    }

    function checkAuth(callback) {
        $.post("/check-auth", {}, function(data) {
            if (data.status == 'success' && data.user) {
                user.userId = data.user.userId;
                user.userName = data.user.username;
                user.count = data.user.count;
                fillUser();
            } else {
                $('#regModal').modal();
            }

            if (callback) {
                callback();
            }
        });
    }

    function click() {
        if (!user.block) {
            user.block = true;
            $.post("/click", {}, function(data) {
                user.block = false;

                if (data.status == 'success') {
                    user.count += Math.floor(data.count);
                    $("#count").html(numberWithCommas(user.count));
                    clickEffect(data.count);
                }
            });
        }
    }

    function clickEffect(count) {
        if (user.timer) {
            clearTimeout(user.timer);
        }

        $(".bonus").removeClass("bonus-active");
        $(".bonus-" + count).addClass("bonus-active");

        $("#click-result").show().html('+' + count);
        user.timer = setTimeout(function() {
            $("#click-result").fadeOut(function() {
                $(this).html('');
            });
            $(".bonus").removeClass("bonus-active");
        }, 3000)
    }

    function getRating() {
        $.get("/rating", {}, function(data) {
            if (data.status == 'success') {
                $("#ratingTable").html('');
                $.each(data.users, function(key, value) {
                    var countNumber = numberWithCommas(Math.floor(value.count));
                    var usernameText = value.username;
                    var countText = countNumber;

                    if (user.userName == value.username) {
                        usernameText = '<b>' + usernameText + '</b> (ТЫ)';
                        countText = '<b>' + countText + '</b>';
                    }

                    $("#ratingTable").append('<tr><td>' + usernameText + '</td><td>' + countText + '</td></tr>');
              });
            }
        });
    }

    function register(username) {
        $.post("/register", {username: username}, function(data) {
                user.userId = data.user._id;
                user.userName = data.user.username;
                user.count = data.user.count;
                fillUser();
                $('#regModal').modal('hide');
        });
    }

    function getCookie(name) {
      var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : false;
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    function numberStr(value) {
        var p = Math.floor(Math.log10(value));

        switch (p) {
            case 1:
            case 2:
                return value;
                break;
            case 3:
                return Number((value / 1000).toFixed(2)) + 'K';
                break;
            case 4:
                return Number((value / 1000 / 1000).toFixed(2)) + 'M';
                break;
            default:
                return value;
        }
    }
});
